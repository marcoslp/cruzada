"""
Clase que define caracteristicas de soldado Barbaro
"""

# -*- coding: utf-8 -*-
from Unidad_de_juego import Unidad_de_juego
from Funciones import cargarJSON


class Barbaro(Unidad_de_juego):
    def __init__(self):
        sold = cargarJSON()
        Unidad_de_juego.__init__(self, sold['nombre_enemigo'])
        self.max_salud = sold['maxSalud_enemigo']
        self.Medidor_salud = self.max_salud
        self.tipo_unidad = sold['tipUnidad_enemigo']
        self.numero_choza = 0
