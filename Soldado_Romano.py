"""
    Funcionalidades del Soldado Romano
        Importamos Unidad_de_juego de Unidad_de_juego.
        Importamos print_bold,Add_Log de Funciones.

        Cargamos los datos desde un JSON.

        En dicha clase encontraremos metodos como:
        consquitar_domus: recibe domus, se encarga de derrollar la batalla,
        eso si hay un enemigo sino tenemos mensajes, para aliados y no ocupada
        Huir: coloca un msj de huida, cuando no queremos batallar más

"""

# -*- coding: utf-8 -*-
from Unidad_de_juego import Unidad_de_juego
from Funciones import print_bold, Add_Log, cargarJSON


class Soldado(Unidad_de_juego):
    def __init__(self):
        sold = cargarJSON()
        Unidad_de_juego.__init__(self, sold['nombre'])
        self.max_salud = sold['maxSalud']
        self.Medidor_salud = self.max_salud
        self.tipo_unidad = sold['tipUnidad']

    def Conquistar_domus(self, domus):
        msg1 = "Entrando en el domus %d..." % domus.numero
        print_bold(msg1)
        Add_Log(msg1)

        es_enemigo = (isinstance(domus.ocupantes, Unidad_de_juego)
                      and domus.ocupantes.tipo_unidad == 'enemigo')
        continuar_ataque = 1

        if es_enemigo:
            msg2 = "¡Enemigo sitiado!"
            print_bold(msg2)
            Add_Log(msg2)

            self.mostrar_salud(bold=True)
            domus.ocupantes.mostrar_salud(bold=True)
            while continuar_ataque:
                continuar_ataque = input("...continuar ataque? Si(1)/No(0)")
                Add_Log(str(continuar_ataque))

                if continuar_ataque == 0:
                    self.huir()
                    break

                self.atacar(domus.ocupantes)

                if domus.ocupantes.Medidor_salud <= 0:
                    print("")
                    domus.conquistar(self)
                    break
                if self.Medidor_salud <= 0:
                    print("")
                    break
        else:
            if domus.Tipo_de_ocupante() == 'No ocupada':
                msg3 = "El domus está vacía"
                print_bold(msg3)
                Add_Log(msg3)

            else:
                msg4 = "¡Enhora Buena un Aliado!"
                print_bold(msg4)
                Add_Log(msg4)

            domus.conquistar(self)
            self.curar()

    def huir(self):
        msg5 = "Escapando del enemigo..."
        print_bold(msg5)
        Add_Log(msg5)
        self.enemigo = None
