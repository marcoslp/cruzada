
"""
    Funciones de impresion de caracteres con un destaque.
    Logica de Ataque aleatorio
    Funcion de manejo de archivo para Log
"""

# -*- coding: utf-8 -*-
import random
import json


def print_bold(msg):
    # Funcion para mostrar por pantalla un string en negrita
    print("\033[1m" + msg + "\033[0m")


def print_linea_punteada(width=72):
    """Funcion que imprimer linea punteada de 72 caracteres de ancho"""
    print('-' * width)


def eleccion_aleatoria_ataque(obj1, obj2):
    """Logica de eleccion aleatoria de ataque"""
    weighted_list = 5 * [id(obj1)] + 5 * [id(obj2)]
    selection = random.choice(weighted_list)

    if selection == id(obj1):
        return obj1

    return obj2


def Add_Log(cadena_log):
    """Func. que almacena logs del juego."""
    archivo_almacenamiento = "Log_Juego.txt"
    Documento = open(archivo_almacenamiento, 'a')
    Documento.write(cadena_log + '\n')
    Documento.close()
    return


def cargarJSON():
    """Funcion que lee Json de configuracion de soldados"""
    ruta = "Json_Conf/conf_soldados.json"
    with open(ruta) as contenido:
        soldados = json.load(contenido)
        for soldado in soldados:
            soldado_conf = soldado
    return soldado_conf
