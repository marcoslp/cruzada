"""
Prueba para testrar la salud y las funciones de salud de un Soldado.
este modelo se implemento a partir de una documentacion.
es una prueba de error especifico al curar
Importamos: print_function, soldado, y GameUnitError

"""

# -*- coding: utf-8 -*-
from __future__ import print_function
from Soldado_Romano import Soldado
from ErrorUnidadJuego import GameUnitError

if __name__ == '__main__':
    print("Creando a un caballero..")
    knight = Soldado("Sir Bar")
    knight.Medidor_salud = 10
    knight.mostrar_salud()
    try:
        knight.curar(curado_por=100, curacion_completa=False)
    except GameUnitError as e:
        print(e)
        print(e.error_message)

    knight.mostrar_salud()
