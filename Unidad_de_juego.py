
"""
    En esta clase desarrollamos metodos:
    Atacar, reset medidores de salud, Curar, mostrar medidores.
"""


# -*- coding: utf-8 -*-
from Funciones import print_bold, Add_Log
from Funciones import eleccion_aleatoria_ataque
import random
from ErrorUnidadJuego import GameUnitError


class Unidad_de_juego:
    def __init__(self, Nombre_unidad=''):
        self.max_salud = 0
        self.Medidor_salud = 0
        self.nombre = Nombre_unidad
        self.enemigo = None
        self.tipo_unidad = None

    def info(self):
        pass

    def atacar(self, enemigo):
        # Funcion que implementamos en el archivo de funciones de juego
        Unidad_herida = eleccion_aleatoria_ataque(self, enemigo)
        herida = random.randint(10, 15)
        Unidad_herida.Medidor_salud = max(Unidad_herida.Medidor_salud - herida,
                                          0)
        msg = "¡Ataque! "
        print(msg)
        Add_Log(msg)
        self.mostrar_salud()
        enemigo.mostrar_salud()

    def curar(self, curado_por=2, curacion_completa=True):
        if self.Medidor_salud == self.max_salud:
            # Caso en el que la salud sea completa
            return

        if curacion_completa:
            self.Medidor_salud = self.max_salud
        else:
            self.Medidor_salud += curado_por
            # Puede ocurrir que se sobre pase la salud máxima
        if self.Medidor_salud > self.max_salud:
            self.Medidor_salud = self.max_salud
            raise GameUnitError("Medidor_salud > max_salud!!", 101)
        msg1 = "¡Has sido curado!"
        print_bold(msg1)
        Add_Log(msg1)
        self.mostrar_salud(bold=True)

    def reset_medidor_salud(self):
        self.Medidor_salud = self.max_salud

    def mostrar_salud(self, bold=False):
        # Se presenta un problema si no tenemos ningun enemigo
        msg3 = "Salud: %s: %d" % (self.nombre, self.Medidor_salud)
        if bold:
            print_bold(msg3)
            Add_Log(msg3)
        else:
            print(msg3)
            Add_Log(msg3)
