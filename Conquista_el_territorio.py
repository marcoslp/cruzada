"""
    Es la clase principal: Importamos Soldados Romano y Barbaro,
    domus, print_bold, Add_Log, random y textwrap.
    tenemos funciones como:
    get_ocupantes,Mostrar_mision, procesar_desición,ocupar_domus y Play
    Ademas de aqui se añade la gran mayoria de Logs del Juego.
"""

# -*- coding: utf-8 -*-
from Soldado_Romano import Soldado
from Soldado_Barbaro import Barbaro
from Domus import Domus
from Funciones import print_bold, Add_Log
import random
import textwrap


class Juego:
    def __init__(self):
        self.domus = []
        # Inicializamos el número de domus
        self.jugador = None
        # En primer lugar el jugador no tendrá una instancia definida

    def get_ocupantes(self):
        return [x.Tipo_de_ocupante() for x in self.domus]

    def Mostrar_mision(self):
        msg = ("Cruzada del Soldado del Imperio V2.0.0")
        Add_Log(msg)
        print_bold(msg)
        msgq = ("La guerra entre Romano y Barbaros a durado ya muchos años.")
        msgq1 = ("Un enorme ejército Barbaro a invadido parte del imperio.")
        msgq2 = ("Destruían prácticamente todo en su camino.")
        msgq3 = (
            "Te han seleccionado como el Soldado Capaz de recuperar esta villa"
        )
        msgq4 = ("Derrota a los Barbaros y reestablece el orden.")
        msgq5 = ("Muchos lo han intentado, se tu el primero,")
        msgq6 = ("en lograr recuperar las tierrras.")
        msgq7 = ("Confiamos en tu destreza y gran habilidad con la espada.")
        msgq8 = ("No sabemos si hay sobrevivientes!!")
        msgq9 = ("POR EL IMPERIO ! HASTA EL FINAL.....")
        print(textwrap.fill(msgq))
        print(textwrap.fill(msgq1))
        print(textwrap.fill(msgq2))
        print(textwrap.fill(msgq3))
        print(textwrap.fill(msgq4))
        print(textwrap.fill(msgq5))
        print(textwrap.fill(msgq6))
        print(textwrap.fill(msgq7))
        print(textwrap.fill(msgq8))
        print(textwrap.fill(msgq9))
        Add_Log(msgq)
        Add_Log(msgq1)
        Add_Log(msgq2)
        Add_Log(msgq3)
        Add_Log(msgq4)
        Add_Log(msgq5)
        Add_Log(msgq6)
        Add_Log(msgq7)
        Add_Log(msgq8)
        Add_Log(msgq9)
        msga = ("Misión:")
        msgb = ("1. Lucha contra los Barbaros en tu camino.")
        msgc = ("2. Recupera los domus invadidos por los barbaros.")
        msgd = ("3. Planta la bandera del Imperior y ve por tu recompensa.")
        print_bold(msga)
        print(msgb)
        print(msgc)
        print(msgd)
        print("-" * 72)
        Add_Log(msga)
        Add_Log(msgb)
        Add_Log(msgc)
        Add_Log(msgd)

    def _procesar_decision(self):
        verifying_choice = True
        idx = 0
        print("Ocupantes actuales: %s" % self.get_ocupantes())
        while verifying_choice:
            user_choice = input("Elige un número de Domus para entrar (1-5): ")
            try:
                idx = int(user_choice)
            except ValueError as e:
                msg8 = "Entrada no válida: %s \n" % e.args
                print(msg8)
                Add_Log(msg8)
                continue
            try:
                if self.domus[idx - 1].recuperada:
                    msg3 = ("Este Domu, fue recuperadaerado !!"
                            "No puedes curarte.")
                    print_bold(msg3)
                    Add_Log(msg3)
                else:
                    verifying_choice = False
            except IndexError:
                msg4 = "Entrada no aceptada" + idx
                msg5 = "El número debe estar entre 1 y 5.Inténtalo de nuevo"
                Add_Log(msg4)
                Add_Log(msg5)
                print("Entrada no aceptada: ", idx)
                print("El número debe estar entre 1 y 5.Inténtalo de nuevo")
                continue
        return idx

    def _ocupar_domus(self):
        for i in range(5):
            ocupantes = ['enemigo', 'amigo', None]
            eleccion_aleatoria = random.choice(ocupantes)

            if eleccion_aleatoria == 'enemigo':
                # Colocamos el numero del enemigo como identificador
                self.domus.append(Domus(i + 1, Barbaro()))
            elif eleccion_aleatoria == 'amigo':
                self.domus.append(Domus(i + 1, Soldado()))
            else:
                self.domus.append(Domus(i + 1, eleccion_aleatoria))

    def play(self):
        self.jugador = Soldado()
        self._ocupar_domus()
        Contador_domus_recuperadas = 0

        self.Mostrar_mision()
        self.jugador.mostrar_salud(bold=True)

        while Contador_domus_recuperadas < 5:
            idx = self._procesar_decision()
            self.jugador.Conquistar_domus(self.domus[idx - 1])

            if self.jugador.Medidor_salud <= 0:
                msg6 = "Has Muerto, que tengas suerte la proxima vez "
                print_bold(msg6)
                Add_Log(msg6)

                break

            if self.domus[idx - 1].recuperada:
                Contador_domus_recuperadas += 1
                Add_Log("Recuperado un Domus")

        if Contador_domus_recuperadas == 5:
            msg7 = "¡Enhorabuena! Has plantado la Bandera del Imperio"
            Add_Log(msg7)
            print_bold(msg7)
            msg12 = "Puedes ir por tu Recompensa con el REY"
            Add_Log(msg12)
            print_bold(msg12)
