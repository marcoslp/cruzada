
"""
Clase y methodos de lo que representarian las casas
Un constructor, un metodo conquistar, y una funcion que determina si esta
recuperada o no.
Se utilizan e importa print_bold
"""

# -*- coding: utf-8 -*-

from Funciones import print_bold, Add_Log


class Domus:
    def __init__(self, numero, ocupantes):
        self.ocupantes = ocupantes
        self.numero = numero
        self.recuperada = False

    def conquistar(self, nuevo_ocupante):
        self.ocupantes = nuevo_ocupante
        self.recuperada = True
        msg = "¡Buena trabajo! el domus %d ha sido Recuperado" % self.numero
        print_bold(msg)
        Add_Log(msg)

    def Tipo_de_ocupante(self):
        if self.recuperada:
            tipo_ocupante = 'Recuperada'
        elif self.ocupantes is None:
            tipo_ocupante = 'No ocupada'
        else:
            tipo_ocupante = self.ocupantes.tipo_unidad

        return tipo_ocupante
